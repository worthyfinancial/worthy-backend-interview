from datetime import date
import json
import unittest

import app
import models


app.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'


single_transaction =  {
   "_account": "DgMjP0paP3fy3j5xww8YipOKBbgwbjuzNPPK3",
   "_id": "NwDbjmpNjaceB6wQKKdgcZJ7g0BLwvcpgv313",
   "amount": 15.75,
   "date": "2016-03-21",
   "name": "FIVERR",
   "meta": {
     "payment_processor": "PayPal",
     "location": {
       "city": "San Jose",
       "state": "CA"
     }
   },
   "pending": False,
   "type": {
     "primary": "special"
   },
   "category": [
     "Transfer",
     "Third Party",
     "PayPal"
   ],
   "category_id": "21010004",
   "score": {
     "location": {
       "city": 1,
       "state": 1
     },
     "name": 1
   }
}


class TestCase(unittest.TestCase):
    def setUp(self):
        app.db.drop_all()
        app.db.create_all()
        self.user = models.User(account_id='DgMjP0paP3fy3j5xww8YipOKBbgwbjuzNPPK3')
        app.db.session.add(self.user)#
        app.db.session.commit()

    def tearDown(self):
        app.db.session.remove()
        app.db.drop_all()

    def test_difference_to_next_dollar(self):
        self.assertEqual(models.difference_to_next_dollar(70), 30)
        self.assertEqual(models.difference_to_next_dollar(21), 79)
        self.assertEqual(models.difference_to_next_dollar(1121), 79)
        self.assertEqual(models.difference_to_next_dollar(100), 100)
        self.assertEqual(models.difference_to_next_dollar(0), 100)

    def test_transaction_schema(self):
        transaction_schema = models.TransactionSchema()
        result = transaction_schema.load(single_transaction)
        self.assertEqual(result.user_id,
                         'DgMjP0paP3fy3j5xww8YipOKBbgwbjuzNPPK3')
        self.assertEqual(result.value_in_cents, 1575)
        self.assertEqual(result.name, 'FIVERR')
        self.assertEqual(result.id, 'NwDbjmpNjaceB6wQKKdgcZJ7g0BLwvcpgv313')
        self.assertEqual(result.date, date(2016, 3, 21))

    def test_transaction_saving_to_db(self):
        transaction_schema = models.TransactionSchema()
        transaction = transaction_schema.load(single_transaction)
        app.db.session.add(transaction)
        app.db.session.commit()

        self.assertEqual(len(self.user.transactions), 1)
        self.assertEqual(self.user.transactions[0].id,
                         'NwDbjmpNjaceB6wQKKdgcZJ7g0BLwvcpgv313')

    def test_import_transactions_from_json(self):
        transaction_schema = models.TransactionSchema()
        with open('files/initial_transactions.json') as f:
            data = json.loads(f.read())

        transactions = transaction_schema.load(data, many=True)
        self.user.add_transactions(transactions)

        self.assertEqual(len(self.user.transactions), 7)
        self.assertEqual(self.user.roundup_cents, 189)

    def test_import_transaction_with_duplicates(self):
        transaction_schema = models.TransactionSchema()

        with open('files/initial_transactions.json') as f:
            data = json.loads(f.read())

        transactions = transaction_schema.load(data, many=True)
        self.user.add_transactions(transactions)

        with open('files/updated_transactions.json') as f:
            data = json.loads(f.read())

        transactions = transaction_schema.load(data, many=True)
        self.user.add_transactions(transactions)

        self.assertEqual(len(self.user.transactions), 15)
        self.assertEqual(self.user.roundup_cents, 773)

